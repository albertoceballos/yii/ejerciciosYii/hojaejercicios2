<?php

namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;

class Foto extends Widget{
    
    public $nombre="foto1.jpg";
    public $alternativo="foto1";
    
    public function init() {
        parent::init();
    }
    
    public function run() {
        return Html::img("@web/imgs/$this->nombre",["alt"=>$this->alternativo,"width"=>"300px"]);
    }
}

