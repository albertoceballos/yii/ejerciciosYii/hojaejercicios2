<?php

use yii\helpers\Html;
use app\components\Foto;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Hoja de ejercicios 2</h1>

        <p class="lead">Esta es la página de inicio.</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-md-4">
                <h2>Izquierda</h2>
                
                <?= Foto::widget(); ?>
            </div>
            <div class="col-md-4">
                <h2>Centro</h2>
                <?= Foto::widget(); ?>
                
            </div>
            <div class="col-md-4">
                <h2>Derecha</h2>
                 <?= Foto::widget(["nombre"=>"foto2.jpg","alternativo"=>"Barcelona"]); ?>
               
            </div>
        </div>

    </div>
</div>
