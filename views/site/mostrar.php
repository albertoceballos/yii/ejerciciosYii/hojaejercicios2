<?php

use yii\grid\GridView;

echo GridView::widget([
   'dataProvider' =>$dataProvider,
   'summary'=>"mostrando {begin}-{end} de {totalCount} elementos",
   'columns'=>[
     ['class'=>'yii\grid\serialColumn'],
       'id',
       'texto',
   ], 
]);
